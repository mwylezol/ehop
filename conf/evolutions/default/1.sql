# --- !Ups

CREATE TABLE Products (
 "prodId" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
 "title" VARCHAR  NOT NULL,
 "description" TEXT NOT NULL,
 "price" INTEGER NOT NULL,
 "catId" INTEGER NOT NULL
);

CREATE TABLE Categories (
 "catId" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
 "title" VARCHAR  NOT NULL
);

CREATE TABLE Orders (
 "orderId" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
 "productIds" TEXT NOT NULL,
 "paymentMethod" VARCHAR NOT NULL,
 "deliveryMethod" VARCHAR NOT NULL,
 "userId" INTEGER NOT NULL
);

CREATE TABLE Users (
 "userId" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
 "username" VARCHAR NOT NULL,
 "passwordHash" VARCHAR NOT NULL
);

INSERT INTO Categories Values(1, "Processors");
INSERT INTO Categories Values(2, "Graphics Cards");
INSERT INTO Categories Values(3, "Motherboards");
INSERT INTO Categories Values(4, "PSU");
INSERT INTO Categories Values(5, "Networking");
INSERT INTO Categories Values(6, "Periphals");

INSERT INTO Products VALUES(1, "AMD RYZEN 1800X", "Brand new ", 99999, 1);
INSERT INTO Products VALUES(2, "Intel i9700", "Super overpriced", 1234567, 1);

INSERT INTO Products VALUES(3, "NVIDIA 1080", "4k Gaming", 112234, 2);
INSERT INTO Products VALUES(4, "AMD RX480", "UHD Gaming", 321321, 2);

INSERT INTO Products VALUES(5, "Gigabyte UD5", "Ultra durable motherboard", 112233, 3);

INSERT INTO Products VALUES(6, "Chieftect 400W", "85+ PLUS", 20000, 4);

INSERT INTO Products VALUES(7, "Network card LAN", "1000mbps Gigabyte", 4000, 5);

# --- !Downs

DROP TABLE Products;
DROP TABLE Categories;
DROP TABLE Orders;
DROP TABLE Users;