package models

import play.api.libs.json.Json

case class ProductsREST(prodId:Long, title: String, description: String, price: Long)

object ProductsREST {
  implicit val productsFormat = Json.format[ProductsREST]
}

case class CategoriesREST(catId: Long, title: String)

object CategoriesREST {
  implicit val categoriesFormat = Json.format[CategoriesREST]
}

case class OrdersREST(orderId: Long, productIds : String, paymentMethod : String, deliveryMethod : String, userId: Long)

object OrdersREST {
  implicit val ordersFormat = Json.format[OrdersREST]
}

case class UsersREST(userId: Long, username: String, passwordHash: String)

object UsersREST{
  implicit val usersFormat = Json.format[UsersREST]
}