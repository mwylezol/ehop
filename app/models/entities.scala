package models

case class Products(prodId: Long, title: String, description: String, price: Long, catId: Long)

case class Categories(catId: Long, title: String)

case class Orders(orderId: Long, productIds: String, paymentMethod: String, deliveryMethod: String, userId: Long)

case class Users(userId: Long, username: String, passwordHash: String)