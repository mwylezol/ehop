package controllers

import javax.inject.Inject

import daos.{CategoriesDao, OrdersDao, ProductsDAO}
import models.{Orders, OrdersREST}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application @Inject() (productsDAO: ProductsDAO, categoriesDao: CategoriesDao, ordersDao: OrdersDao) extends Controller {

  def index = Action.async { implicit request =>
    productsDAO.all map {
      products => Ok(Json.toJson(products))
    }
  }

  def productFromCategory(catId: Long) = Action.async { implicit request =>
      productsDAO.allFromCategory(catId) map {
      products => Ok(Json.toJson(products))
    }
  }

  def categories = Action.async { implicit request =>
    categoriesDao.all map {
      categories => Ok(Json.toJson(categories))
    }
  }

  def orders = Action.async { implicit request =>
    ordersDao.all map {
      orders => Ok(Json.toJson(orders))
    }
  }

  def newOrder = Action { implicit request =>
    var json:OrdersREST = request.body.asJson.get.as[OrdersREST]
    var order = Orders(orderId = 0, productIds = json.productIds, deliveryMethod = json.deliveryMethod, paymentMethod = json.deliveryMethod, userId = json.userId)
    ordersDao.insert(order)
    Ok(request.body.asJson.get)
  }
}
