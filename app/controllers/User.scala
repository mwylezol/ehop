package controllers

import javax.inject.Inject

import daos.UsersDao
import models.{Users, UsersREST}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Await
import scala.concurrent.duration._

class User @Inject()(usersDao: UsersDao) extends Controller {

  def index = Action.async { implicit request =>
    usersDao.all map {
      products => Ok(Json.toJson(products))
    }
  }

  def register = Action { implicit request =>
    var json: UsersREST = request.body.asJson.get.as[UsersREST]
    var user = Users(userId = 0, username = json.username, passwordHash = json.passwordHash)
    usersDao.insert(user)
    Ok(request.body.asJson.get)
  }

  def login = Action { implicit request =>
    var json: UsersREST = request.body.asJson.get.as[UsersREST]
    var user = Await.result(usersDao.find(json.username), 10 seconds)

    if (user != null && user.passwordHash == json.passwordHash) {
      Ok
    } else {
      NotFound
    }
  }
}
