package daos

import javax.inject.Inject

import models.{Orders, OrdersREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

class OrdersDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Orders = TableQuery[OrdersTable]

  def all(implicit ec: ExecutionContext): Future[List[OrdersREST]] = {
    val query = Orders
    val results = query.result
    val futureOrders = db.run(results)
    futureOrders.map(
      _.map {
        a => OrdersREST(orderId = a.orderId, productIds = a.productIds, paymentMethod = a.paymentMethod, deliveryMethod = a.deliveryMethod, userId = a.userId)
      }.toList)
  }

  def insert(order: Orders): Future[Unit] = {
    db.run(Orders += order).map { _ => () }
  }

  class OrdersTable(tag: Tag) extends Table[Orders](tag, "Orders") {
    def orderId = column[Long]("orderId",O.AutoInc, O.AutoInc)
    def productIds = column[String]("productIds")
    def paymentMethod = column[String]("paymentMethod")
    def deliveryMethod = column[String]("deliveryMethod")
    def userId = column[Long]("userId")
    def * = (orderId, productIds, paymentMethod, deliveryMethod, userId) <> (models.Orders.tupled, models.Orders.unapply)
  }
}
