package daos

import javax.inject.Inject

import models.{Categories, CategoriesREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

class CategoriesDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Categories = TableQuery[CategoriesTable]

  def all(implicit ec: ExecutionContext): Future[List[CategoriesREST]] = {
    val query = Categories
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
        a => CategoriesREST(catId = a.catId, title = a.title)
      }.toList)
  }

  def insert(category: Categories): Future[Unit] = db.run(Categories += category).map { _ => () }

  class CategoriesTable(tag: Tag) extends Table[Categories](tag, "Categories") {
    def catId = column[Long]("catId",O.AutoInc, O.AutoInc)
    def title = column[String]("title")
    def * = (catId, title) <> (models.Categories.tupled, models.Categories.unapply)
  }

}
