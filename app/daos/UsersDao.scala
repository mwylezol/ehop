package daos

import javax.inject.Inject

import models.{UsersREST, Users}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

class UsersDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  def all(implicit ec: ExecutionContext): Future[List[UsersREST]] = {
    val query = TableQuery[UsersTable]
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
        a => UsersREST(userId = a.userId, username = a.username, passwordHash = a.passwordHash)
      }.toList)
  }

    def find(username: String): Future[UsersREST] = {
      val query = TableQuery[UsersTable].filter(u => u.username === username)
      val results = query.result
      val futureProducts = db.run(results)
      futureProducts.map(
        _.map {
          a => UsersREST(userId = a.userId, username = a.username, passwordHash = a.passwordHash)
        }.toList.head)
    }

    def insert(user: Users): Future[Unit] = db.run(TableQuery[UsersTable] += user).map { _ => () }

    class UsersTable(tag: Tag) extends Table[Users](tag, "Users") {
      def userId = column[Long]("userId",O.AutoInc, O.AutoInc)
      def username = column[String]("username")
      def passwordHash = column[String]("passwordHash")
      def * = (userId, username, passwordHash) <> (models.Users.tupled, models.Users.unapply)
    }

}
