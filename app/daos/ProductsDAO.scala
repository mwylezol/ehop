package daos

import javax.inject.Inject

import models.{Products, ProductsREST}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

class ProductsDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile] {

  import driver.api._

  val Products = TableQuery[ProductsTable]

  def all(implicit ec: ExecutionContext): Future[List[ProductsREST]] = {
    val query = Products
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
        a => ProductsREST(prodId = a.prodId, description = a.description, title = a.title, price = a.price)
      }.toList)
  }

  def allFromCategory(categoryId: Long) : Future[List[ProductsREST]] = {
    val query = TableQuery[ProductsTable].filter(p => p.catId === categoryId)
    val results = query.result
    val futureProducts = db.run(results)
    futureProducts.map(
      _.map {
        a => ProductsREST(prodId = a.prodId, description = a.description, title = a.title, price = a.price)
      }.toList)
  }

  class ProductsTable(tag: Tag) extends Table[Products](tag, "Products") {
    def prodId = column[Long]("prodId",O.AutoInc, O.AutoInc)
    def title = column[String]("title")
    def description = column[String]("description")
    def catId = column[Long]("catId")
    def price = column[Long]("price")
    def * = (prodId, title, description, price, catId) <> (models.Products.tupled, models.Products.unapply)
  }

}
