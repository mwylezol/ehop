export class Product {
  prodId: string;
  title: string;
  description: string;
  price: number;
}
