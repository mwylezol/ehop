import { Component, OnInit, OnDestroy } from '@angular/core';
import {ProductService} from './product.service';
import {Product} from './product';
import {ActivatedRoute} from '@angular/router';
import {CartService} from '../cart/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy  {

  products: Product[];
  catId: number;
  private sub: any;

  constructor(private productService: ProductService, private cartService: CartService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.catId = +params['catId'];

      console.log('products from category:' + this.catId);
      if (isNaN(this.catId)) {
        this.productService.getProducts().subscribe(data => this.products = data);
      } else {
        console.log('trying to call for products from category:' + this.catId);
        this.productService.getProductsByCatId(this.catId).subscribe(data => this.products = data);
      }
      console.log(this.route.snapshot.params);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  public addToCart(product: Product) {
    this.cartService.addToCart(product);
  }
}
