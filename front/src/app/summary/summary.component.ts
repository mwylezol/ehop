import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CartService} from '../cart/cart.service';
import {Product} from '../product/product';
import {of} from 'rxjs/observable/of';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Order} from './order';
import {SummaryService} from './summary.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  public cartItems$: Observable<Product[]> = of([]);
  public cartItems: Product[] = [];
  orderForm: FormGroup;

  constructor(private cartService: CartService, private summaryService: SummaryService) {
    this.cartItems$ = this
      .cartService
      .getItems();

    this.orderForm = new FormGroup({
      deliveryMethod: new FormControl('deliveryMethod', Validators.required),
      paymentMethod: new FormControl('paymentMethod', Validators.required)
    });

    this.cartItems$.subscribe(_ => this.cartItems = _);
  }

  ngOnInit() {
  }

  addOrder(event) {
    console.log(event);
    const newOrder = new Order(0,
      this.cartItems.map(c => c.prodId).join(','),
      this.orderForm.get('deliveryMethod').value,
      this.orderForm.get('paymentMethod').value, 0);
    this.summaryService.sendToPlay(newOrder);
    this.cartService.removeAllFromCart();
  }
}
