export class Order {
  orderId: number;
  productIds: string;
  paymentMethod: string;
  deliveryMethod: string;
  userId: number;

  constructor(newOrderID: number, newProductIds: string, newDeliveryMethod: string, newPaymentMethod: string, newUserId: number) {
    this.orderId = newOrderID;
    this.productIds = newProductIds;
    this.deliveryMethod = newDeliveryMethod;
    this.paymentMethod = newPaymentMethod;
    this.userId = newUserId;
  }
}
