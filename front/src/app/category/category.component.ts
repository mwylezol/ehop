import { Component, OnInit } from '@angular/core';
import {CategoryService} from './category.service';
import {Category} from './category';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories: Category[];

  constructor(private categoryService: CategoryService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(data => this.categories = data);

    console.log(this.route.snapshot.params);
  }

}
