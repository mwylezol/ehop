import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import {RouterModule} from '@angular/router';
import {ProductService} from './product/product.service';
import {CategoryComponent} from './category/category.component';
import {CategoryService} from './category/category.service';
import {CartComponent } from './cart/cart.component';
import {CartService} from './cart/cart.service';
import {Location, CommonModule} from '@angular/common';
import { SummaryComponent } from './summary/summary.component';
import {SummaryService} from "./summary/summary.service";
import { AlertModule } from 'ngx-bootstrap';
import { SuccessComponent } from './success/success.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CategoryComponent,
    CartComponent,
    SummaryComponent,
    SuccessComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    RouterModule.forRoot([
      { path: '.', component: AppComponent},
      { path: 'products', component: ProductComponent},
      { path: 'productsFromCat/:catId', component: ProductComponent},
      { path: 'categories', component: CategoryComponent},
      { path: 'cart', component: CartComponent},
      { path: 'checkout', component: SummaryComponent},
      { path: 'success', component: SuccessComponent},
      { path: 'login', component: LoginComponent},
      { path: 'register', component: RegisterComponent}
      ])
  ],
  providers: [ProductService, CategoryService, CartService, Location, SummaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
