import {Component, OnInit} from '@angular/core';
import {CartService} from './cart.service';
import {Product} from '../product/product';
import {Observable} from 'rxjs';
import {of} from 'rxjs/observable/of';

@Component({
  selector: 'app-cart-component',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public cartItems$: Observable<Product[]> = of([]);
  public cartItems: Product[] = [];

  constructor(private cartService: CartService) {
    this.cartItems$ = this
      .cartService
      .getItems();

    this.cartItems$.subscribe(_ => this.cartItems = _);
  }

  ngOnInit() {
  }

  public getTotal(): Observable<number> {
    return this.cartService.getTotalAmount();
  }

  public removeItem(item: Product) {
    this.cartService.removeFromCart(item);
  }

}
